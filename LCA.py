# Python Program for Lowest Common Ancestor in a Directed Acyclic Graph

from collections import defaultdict


class DAG:
    def __init__(self, vertices):
        # self.V = vertices  # Number of vertices

        # Key = A Node
        # Value = List of Nodes adjacent to that Node
        self.graph = defaultdict(list)      # Dictionary containing adjacency list

        # Set upon addition of edges using addEdge() method
        self.ins = defaultdict(int)         # Dictionary containing in-degree of each vertex
        self.outs = defaultdict(int)        # Dictionary containing out-degree of each vertex

        # Set by findLCA() method
        self.depths = defaultdict(int)      # Dictionary containing depth of each vertex
        self.ancestors = defaultdict(list)    # Dictionary containing parents of current vertex

    # Function to add an edge to the graph
    def addEdge(self, parent, child):
        self.graph[parent].append(child)

        if child in self.ins.keys():
            self.ins[child] += 1
        else:
            self.ins[child] = 1

        if parent in self.outs.keys():
            self.outs[parent] += 1
        else:
            self.outs[parent] = 1

    # A function used by DFS
    def DFSUtil(self, v, target, depth, visited):

        # Mark the current node as visited
        visited.add(v)

        if v not in self.depths.keys():
            self.depths[v] = depth
        else:
            if depth > self.depths[v]:
                self.depths[v] = depth

        if v == target:
            if v not in self.ancestors[target]:
                self.ancestors[target].append(v)

            return

        # Recur for all the vertices adjacent to this vertex
        for neighbour in self.graph[v]:
            if (neighbour not in visited) or (depth >= self.depths[neighbour]):
                self.DFSUtil(neighbour, target, depth + 1, visited)

            if neighbour in self.ancestors[target] and v not in self.ancestors[target]:
                self.ancestors[target].append(v)

    # The function to do DFS traversal. It uses
    # recursive DFSUtil()
    def DFS(self, v, target):

        # Create a set to store visited vertices
        visited = set()

        # Call the recursive helper function to print DFS traversal
        self.DFSUtil(v, target, 0, visited)

    def findLCA(self, source, n1, n2):

        # Reset ancestors and depths
        self.reset_ancestors()
        self.reset_depths()

        if source not in self.graph.keys():
            return [-1]

        # Set ancestors of n1
        self.DFS(source, n1)

        # Set ancestors of n2
        self.DFS(source, n2)

        common_ancestors = []

        for anc_n1 in self.ancestors[n1]:
            if anc_n1 in self.ancestors[n2]:
                common_ancestors.append(anc_n1)

        if not common_ancestors:
            return [-1]

        # LCA list initialised with the first of the common ancestors
        lca = [common_ancestors[0]]

        for ca in common_ancestors:
            # If a common ancestor is of greater depth (i.e. "lower") than the current LCA:
            #   It becomes the new LCA
            if self.depths[ca] > self.depths[lca[0]]:
                lca = [ca]

            # Else if a common ancestor is of depth equal to the current LCA:
            #   We add it as one in the list of Lowest Common Ancestors
            else:
                if (self.depths[ca] == self.depths[lca[0]]) and (ca not in lca):
                    lca.append(ca)

            lca.sort()

        return lca

    def reset_depths(self):
        self.depths.clear()

    def reset_ancestors(self):
        self.ancestors.clear()
