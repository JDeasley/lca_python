# Lowest Common Ancestor (LCA) #

Lowest Common Ancestor program, implemented using a Directed Acyclic Graph (DAG) structure in Python, with accompanying Python unittest tests.
