import unittest

from LCA import DAG


class TestLCAMethods(unittest.TestCase):

    def test_findLCA(self):
        dag = DAG(6)

        # Base case: Source is not in the graph
        self.assertEqual(dag.findLCA(1, 3, 4), [-1], "LCA of 2 non-existent nodes in an empty graph")

        dag.addEdge(0, 1)
        dag.addEdge(0, 2)
        dag.addEdge(1, 3)
        dag.addEdge(1, 2)
        dag.addEdge(2, 4)
        dag.addEdge(2, 5)
        dag.addEdge(2, 3)
        dag.addEdge(3, 4)
        dag.addEdge(4, 5)

        self.assertEqual(dag.findLCA(0, 3, 4), [3], "LCA of nodes 3 and 4, with source node 0")
        self.assertEqual(dag.findLCA(0, 1, 2), [1], "LCA of nodes 1 and 2, with source node 0")
        self.assertEqual(dag.findLCA(0, 3, 2), [2], "LCA of nodes 3 and 2, with source node 0")
        self.assertEqual(dag.findLCA(0, 3, 5), [3], "LCA of nodes 3 and 5, with source node 0")

        self.assertEqual(dag.findLCA(0, 1, 2), [1], "LCA of nodes 3 and 5, with source node 0")

        dag2 = DAG(6)

        # New DAG for further edge case tests

        # Edge case: 2 real nodes that are disconnected, i.e. have no common ancestors
        dag2.addEdge(0, 1)
        dag2.addEdge(2, 3)
        self.assertEqual(dag2.findLCA(0, 2, 3), [-1], "LCA of 2 real nodes that have no common ancestors")

        # Edge case: 2 real nodes that have multiple LCAs
        dag2.addEdge(0, 2)
        dag2.addEdge(1, 3)
        dag2.addEdge(1, 4)
        dag2.addEdge(2, 4)

        # 1 and 2 are equally valid LCAs of 3 and 4, so LCA(3,4) = [1,2]
        self.assertEqual(dag2.findLCA(0, 3, 4), [1, 2], "LCA of 2 real nodes with multiple LCAs")

        # Edge case: Source node other than 0
        # Our two nodes, 3 and 4, which previously had 2 LCAs, should now only have 1,
        #   as there is nothing to point to 0 or 2
        self.assertEqual(dag2.findLCA(1, 3, 4), [1], "LCA of 2 real nodes with source node 1")


if __name__ == '__main__':
    unittest.main()
